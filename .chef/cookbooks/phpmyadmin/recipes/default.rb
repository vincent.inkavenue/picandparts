# Installation des paquets
package "phpmyadmin"

# Configuration
template "/etc/apache2/conf.d/phpmyadmin.conf" do
  #template "/etc/phpmyadmin/apache.conf" do
  source "phpmyadmin.conf.erb"
end

# Prise en compte des changements
service "apache2" do
  action :restart
end
