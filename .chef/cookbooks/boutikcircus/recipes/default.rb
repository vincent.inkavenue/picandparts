# Installation des éléments de base
include_recipe 'php'
include_recipe 'apc'
include_recipe 'apache2'
include_recipe 'mysql'
include_recipe 'phpmyadmin'

# Configuration de Magento
### Mise en place de la base de donnée

mysql_connect = "mysql -uroot -p#{node['mysql']['pass_root']}"
queries = [
    "#{mysql_connect} -e 'DROP DATABASE IF EXISTS magento'",
    "#{mysql_connect} -e 'CREATE DATABASE magento'",
    "tar xzf magento.sql.tar.gz",
    "#{mysql_connect} magento < magento.sql",
    "rm magento.sql",
    "#{mysql_connect} -e 'UPDATE magento.#{node['mysql']['table_prefix']}core_config_data 
                            SET value = \"http://#{node['general']['domain']}/\"
                            WHERE path LIKE \"%secure/base_url%\"';",
    "#{mysql_connect} -e 'UPDATE magento.#{node['mysql']['table_prefix']}core_config_data 
                            SET value = 0
                            WHERE path = \"dev/js/merge_files\"';",
    "#{mysql_connect} -e 'UPDATE magento.#{node['mysql']['table_prefix']}core_config_data 
                            SET value = 0
                            WHERE path = \"dev/css/merge_css_files\"';",
]

queries.each do |query|
    execute query do
        cwd "/vagrant/.chef/data/"
        command query
        only_if { ::File.file?("/vagrant/.chef/data/magento.sql.tar.gz")}
    end
end

### Mise en place du local.xml
template "/vagrant/app/etc/local.xml" do
	source "local.xml.erb"
	mode 0644
end