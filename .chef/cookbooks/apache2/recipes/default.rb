# Installation des paquets
package "apache2"

# Configuration des hosts
template "/etc/apache2/sites-available/default" do
  source "default-sites.erb"
end

# Activation des modules
execute "a2enmod rewrite"
execute "a2enmod expires"
execute "a2enmod headers"

# Prise en compte des changements
service "apache2" do
  action :restart
end
