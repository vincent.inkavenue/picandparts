<?php

/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category    Ced;
 * @package     Ced_CsMarketplace
 * @author 		CedCommerce Core Team <coreteam@cedcommerce.com>
 * @copyright   Copyright CedCommerce (http://cedcommerce.com/)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Ced_CsMarketplace_VebayController extends Ced_CsMarketplace_Controller_AbstractController {


    public function indexAction() {
        if(!$this->_getSession()->getVendorId()) return;
        $this->loadLayout ();
        $this->_initLayoutMessages ( 'customer/session' );
        $this->_initLayoutMessages ( 'catalog/session' );
        $this->getLayout ()->getBlock ( 'head' )->setTitle ( $this->__ ( 'Ebay Import' ) );
        $navigationBlock = $this->getLayout()->getBlock('csmarketplace_vendor_navigation');
        if ($navigationBlock) {
            $navigationBlock->setActive('csmarketplace/vebay/index');
        }


        $this->renderLayout ();
    }

    public function saveAction() {
        if(!$this->_getSession()->getVendorId()) return;
        $this->loadLayout ();
        $this->_initLayoutMessages ( 'customer/session' );
        $this->_initLayoutMessages ( 'catalog/session' );
        $this->getLayout ()->getBlock ( 'head' )->setTitle ( $this->__ ( 'Ebay Import' ) );
        $navigationBlock = $this->getLayout()->getBlock('csmarketplace_vendor_navigation');
        if ($navigationBlock) {
            $navigationBlock->setActive('csmarketplace/vebay/index');
        }

        $from = strtotime($_REQUEST['DateFrom']);
        $to = strtotime($_REQUEST['DateTo']);
        $diff = floor(($to - $from)/ (60 * 60 * 24));
        if ($diff > 121) {
            $this->_getSession()->setErrorMessage(true);
            Mage::getSingleton('core/session')->addError('The time range between time from and time to has excedeed 121 days');
            //$this->_redirect('*/*/index');
            //return;
        }

        $this->renderLayout ();
    }


    public function requestPostAction() {
        $params = $this->getRequest()->getParams();
        $collection = Mage::getSingleton('core/session')->getData('ebay_api_response');
        $helper = Mage::helper('csmarketplace/vebay');
        if ($itemsIds = $params['itemids']) {
            foreach (explode(",", $itemsIds) as $itemId) {
               if (isset($collection[$itemId])) {
                   $product = $helper->importItem($collection[$itemId]);

                   $productData = array();
                   $productData['product'] = array();
                   $productData['product']['name'] = $product->getName()?$product->getName():'';
                   $productData['product']['description'] = $product->getData('description')?$product->getData('description'):'';
                   $productData['product']['short_description'] = $product->getData('short_description')?$product->getData('short_description'):'';
                   $productData['product']['sku'] = $product->getSku();
                   $productData['product']['weight'] = $product->getWeight();
                   $productData['product']['stock_data']['qty'] = 1;
                   $productData['product']['stock_data']['is_in_stock'] = 1;
                   $productData['product']['stock_data']['price'] = 1;
                   $productData['product']['stock_data']['special_price'] = 1;

                   Mage::getModel('csmarketplace/vproducts')->processPostSave('new',$product,$productData);
                   //Mage::getModel('csmarketplace/vproducts')->setStoreId($product->getStoreId())->setProductData($product)->saveProduct($this->mode);
               }
            }

/*            $this->loadLayout ();
            $this->_initLayoutMessages ( 'customer/session' );
            $this->_initLayoutMessages ( 'catalog/session' );
            $this->renderLayout ();*/
            $result = array();
            $result['content'] = 'super';
            $this->getResponse()->clearHeaders()->setHeader('Content-type','application/json',true);
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
        } else {
            $this->_getSession()->addError('No selected items');
            $this->_redirect('*/*/index');
            return;
        }
    }
}
