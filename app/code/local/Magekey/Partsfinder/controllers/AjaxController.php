<?php

class Magekey_Partsfinder_AjaxController extends Mage_Core_Controller_Front_Action
{
    public function autoloadAction()
    {
        $compatibilities = Mage::getModel('partsfinder/compatibility')->getCollection()->load();

        $data     = $this->getRequest()->getPost();
        $selected = $data['selected'];

        if (isset($data['row'])) {
            $row = $data['row'];
            $marque = $data['marque'][$row];
            if (isset($data['model'])) {
                $model = $data['model'][$row];
            }
        } else {
            $marque = $data['marque'];
            if (isset($data['model'])) {
                $model = $data['model'];
            }
        }

        $results = array();
        $datas   = array();
        switch ($selected) {
            /*case 'select-year':
                $results = Mage::helper('partsfinder/vehicules')->getModeles($data['marque']);
                foreach ($results as $r)
                {
                    $datas['vehicules'][] = array(
                        'libelle' => $r['libelle'],
                        'value'   => $r['libelle']
                    );
                }
                break;*/
            case 'select-model':

                if ($results = Mage::helper('partsfinder/vehicules')->getYears($marque, $model)) {
                    foreach ($results as $r) {
                        $datas['vehicules'][] = array(
                            'libelle' => $r,
                            'value' => $r
                        );
                    }
                } else {
                    $datas['vehicules'][] = array();
                }
                break;
            case 'select-marque':
                if ($results = Mage::helper('partsfinder/vehicules')->getModels($marque)) {
                    foreach ($results as $r) {
                        $datas['vehicules'][] = array(
                            'libelle' => $r,
                            'value' => $r
                        );
                    }
                } else {
                    $datas['vehicules'][] = array();
                }
                break;
            case 'select-color':
                $results = Mage::helper('partsfinder/vehicules')->getTypePiece();
                foreach ($results as $r)
                {
                    $datas['vehicules'][] = array(
                        'libelle' => $r['label'],
                        'value'   => $r['label']
                    );
                }
                break;
            case 'select-typePiece':
                $results = Mage::helper('partsfinder/vehicules')->getEtatPiece();
                foreach ($results as $r)
                {
                    $datas['vehicules'][] = array(
                        'libelle' => $r['label'],
                        'value'   => $r['label']
                    );
                }
                break;
        }

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($datas));
    }

}
?>