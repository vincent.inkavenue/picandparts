<?php
class Magekey_Partsfinder_Model_Resource_Compatibility_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('partsfinder/compatibility');
    }
}