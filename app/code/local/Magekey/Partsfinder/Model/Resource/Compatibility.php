<?php
class Magekey_Partsfinder_Model_Resource_Compatibility extends Mage_Core_Model_Resource_Db_Abstract{

    /**
     * Resource initialization
     */
    public function _construct()
    {
        $this->_init('partsfinder/compatibility', 'id');
    }

}