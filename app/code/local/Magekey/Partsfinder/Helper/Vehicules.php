<?php

class Magekey_Partsfinder_Helper_Vehicules extends Mage_Core_Helper_Abstract {

    public function getMarques()
    {
        return Mage::getModel('catalog/resource_eav_attribute')
            ->loadByCode(Mage_Catalog_Model_Product::ENTITY, 'marque')
            ->getSource()
            ->getAllOptions();

    }

    public function getModeles($idMarque)
    {
        $attr = Mage::getModel('catalog/product')->getResource()->getAttribute('marque');
        $idMarque = $attr->getSource()->getOptionId($idMarque);

        $collection = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToSelect('model')
            ->addAttributeToFilter('marque', $idMarque)
            ->load();

        $productModel = Mage::getModel('catalog/product');
        $attr = $productModel->getResource()->getAttribute('model');

        $modelsUsed = array();
        $modeles = array();
        foreach($collection as $product) {
            $model_id = $product->getData('model');
            if (!in_array($model_id, $modelsUsed)) {
                $modele_label = $attr->getSource()->getOptionText($model_id);
                $modeles[] = array('value' => $model_id, 'libelle' => $modele_label);
                array_push($modelsUsed, $model_id);
            }
        }

        return $modeles;
    }



    public function getColors()
    {
        return Mage::getModel('catalog/resource_eav_attribute')
            ->loadByCode(Mage_Catalog_Model_Product::ENTITY, 'color')
            ->getSource()
            ->getAllOptions();
    }

    public function getTypePiece()
    {
        return Mage::getModel('catalog/resource_eav_attribute')
            ->loadByCode(Mage_Catalog_Model_Product::ENTITY, 'type_piece')
            ->getSource()
            ->getAllOptions();
    }


    public function getEtatPiece()
    {
        return Mage::getModel('catalog/resource_eav_attribute')
            ->loadByCode(Mage_Catalog_Model_Product::ENTITY, 'etat_piece')
            ->getSource()
            ->getAllOptions();
    }

    public function getSkuByMakeYearModel($make='', $model='', $year='') {
        $compatibilities = Mage::getModel('partsfinder/compatibility')->getCollection()
            ->addFieldToSelect('product_id')
            ->addFieldToFilter('make', $make)
            ->addFieldToFilter('model', $model)
            ->addFieldToFilter('years', array('like' => '%'.$year.'%'))
            ->load();

        if (count($compatibilities)) {
            $productId = $compatibilities->getFirstItem()->getProductId();
            if ($product = Mage::getModel('catalog/product')->load($productId)) {
                return $product->getSku();
            }
            return false;
        }
        return false;
    }

    public function getMakes() {
        $compatibilities = Mage::getModel('partsfinder/compatibility')
            ->getCollection()
            ->addFieldToSelect('make')
            ->load();

        $makes = array();
        foreach($compatibilities as $compatibility) {
            if (!in_array($compatibility->getMake(), $makes)) {
                $makes[] = $compatibility->getMake();
            }
        }
        return $makes;
    }

    public function getModels($make) {
        $compatibilities = Mage::getModel('partsfinder/compatibility')
            ->getCollection()
            ->addFieldToSelect('model')
            ->addFieldToFilter('make', array('like' => '%'.$make.'%'))
            ->load();

        $models = array();
        foreach($compatibilities as $compatibility) {
            if (!in_array($compatibility->getModel(), $models)) {
                $models[] = $compatibility->getModel();
            }
        }
        return $models;
    }

    public function getYears($make, $model) {
        $compatibilities = Mage::getModel('partsfinder/compatibility')
            ->getCollection()
            ->addFieldToSelect('years')
            ->addFieldToFilter('make', array('like' => '%'.$make.'%'))
            ->addFieldToFilter('model', array('like' => '%'.$model.'%'))
            ->load();

        if(count($compatibilities)) {
            $years = $compatibilities->getFirstItem()->getYears();
            return explode(",", $years);
        }
        return array();
    }

}