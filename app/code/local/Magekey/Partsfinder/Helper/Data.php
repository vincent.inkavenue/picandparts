<?php

class Magekey_Partsfinder_Helper_Data extends Mage_CatalogSearch_Helper_Data
{

    /**
     * Retrieve search query text
     *
     * @return string
     */
    public function getQueryText()
    {
        if (!isset($this->_queryText)) {
            $this->_queryText = $this->_getRequest()->getParam($this->getQueryParamName());
            if ($this->_queryText === null) {

                $_request = $this->_getRequest();

                if ($_request->getParam('marque')) {
                    $makeYearModel = $_request->getParam('marque');
                    if ($_request->getParam('model')) {
                        $makeYearModel .= ' # ' . $_request->getParam('model');
                    }
                    if ($_request->getParam('annee_fabrication')) {
                        $makeYearModel .= ' # ' . $_request->getParam('annee_fabrication');
                    }
                    $this->_queryText = '|' . $makeYearModel . ' | ' . $_request->getParam('marque'). ' | ' . $_request->getParam('model'). ' | ' . $_request->getParam('annee_fabrication');
                } else if ($_request->getParam('partnumber')) {
                    $this->_queryText = '|' . $_request->getParam('partnumber') . '|';
                }
            }

            if ($this->_queryText === null) {
                $this->_queryText = '';
            } else {
                /* @var $stringHelper Mage_Core_Helper_String */
                $stringHelper = Mage::helper('core/string');
                $this->_queryText = is_array($this->_queryText) ? ''
                    : $stringHelper->cleanString(trim($this->_queryText));

                $maxQueryLength = $this->getMaxQueryLength();
                if ($maxQueryLength !== '' && $stringHelper->strlen($this->_queryText) > $maxQueryLength) {
                    $this->_queryText = $stringHelper->substr($this->_queryText, 0, $maxQueryLength);
                    $this->_isMaxLength = true;
                }
            }
        }
        return $this->_queryText;
    }

    public function getCategories($categoriesArray = null)
    {
        $categories = array();
        if (is_null($categoriesArray)) {
            $categoriesArray = Mage::helper('catalog/category')->getStoreCategories('name', false, true);
        }
        foreach ($categoriesArray->getNodes() as $children) {
            $categories[] = array('id' => (int)$children->getId(), 'name' => $this->getCategoryName($children));
            if ($children->hasChildren()) {
                $categories = array_merge($categories, $this->getCategories($children->getChildren()));
            }
        }
        return $categories;
    }

    protected function getCategoryName($category)
    {
        $level = (int)$category->getLevel();
        $name = "";
        for ($i = 2; $i < $level; $i++) {
            $name .= "&#160;&#160;";
        }
        $name .= "- " . $category->getName();
        return $name;
    }

    function setOrAddOptionAttribute($product, $arg_attribute, $arg_value, $add = true)
    {
        $attribute_model = Mage::getModel('eav/entity_attribute');
        $attribute_options_model = Mage::getModel('eav/entity_attribute_source_table');
        $attribute_code = $attribute_model->getIdByCode('catalog_product', $arg_attribute);
        $attribute = $attribute_model->load($attribute_code);

        $attribute_options_model->setAttribute($attribute);
        $options = $attribute_options_model->getAllOptions(false);

        $value_exists = false;

        // determine if this option exists
        foreach ($options as $option) {
            if ($option['label'] == $arg_value) {
                $value_exists = true;
                break;
            }
        }

        // if this option does not exist, add it.
        if (!$value_exists) {
            $attribute->setData('option', array(
                'value' => array(
                    'option' => array($arg_value, $arg_value)
                )
            ));
            $attribute->save();
        }

        $optionsIds = $this->getAttributeOptionValue($product, $arg_attribute, $arg_value, $add);

        $product->setData($arg_attribute, implode(",", $optionsIds));
        return $product;
    }


    public function getAttributeOptionValue($product, $arg_attribute, $arg_value, $add)
    {
        $attribute_model = Mage::getModel('eav/entity_attribute');
        $attribute_options_model = Mage::getModel('eav/entity_attribute_source_table');
        $attribute_code = $attribute_model->getIdByCode('catalog_product', $arg_attribute);
        $attribute = $attribute_model->load($attribute_code);
        $attribute_options_model->setAttribute($attribute);
        $options = $attribute_options_model->getAllOptions(false);

        if ($add && !is_null($product->getData($arg_attribute))) {
            $optionsIds = explode(",", $product->getData($arg_attribute));
        } else {
            $optionsIds = array();
        }

        foreach ($options as $option) {
            if ($option['label'] == $arg_value) {
                $optionsIds[] = $option['value'];
            }
        }
        if (count($optionsIds)) {
            return $optionsIds;
        }
        return false;
    }


}